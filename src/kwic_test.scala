
import java.io.{File, FileNotFoundException}
import java.nio.file.{Files, Paths}
import org.scalatest.{BeforeAndAfterEach, FlatSpec}
import scala.io.Source

/**
  * These are the tests for the KWIC program. It was specifically mentioned that thorough testing was not required
  * in the specification. It tests the following reasonably thoroughly:
  * -getStopWords
  * -getKeywordsFromFile
  * -printKeywordsOnLine
  *
  * The getKWICFromFile function is tested that it combines the previous functions correctly to give an output, and tests
  * necessary border cases such as files not found. It relies upon the underlying testing for the 3 highlighted functions above
  * for different instances.
  *
  * The updateKWIC function is tested that it updates the file and appends correctly, combines other files correctly,
  * and returns errors on file not found and outputs what is expected. It relies on the testing of the other functions highlighted above
  * for differences in variations of type of file.
  *
  * The getFilesFromUser function was tested manually, that it output the files in the right order and None when no files selected.
  * This was because it relies on human interaction with the GUI as was therefore hard to test automatically, and was a simple function.
  *
  * The runKWIC function, along with the running of the "App", were tested manually due to the manual requirement of the GUI, relying upon
  * the automated tests for the underlying functions. This is a very small gap in coverage as it is just checking that it runs the
  * inserts the manual input into the underlying updateKWIC function. The underlying function has been tested.
  *
  */

class kwic_test extends FlatSpec with BeforeAndAfterEach {

  val testLoc = "testFiles" + File.separator
  val w = "word"

  val printer: Kwic.pType = Kwic.printKeywordsOnLine
  val finder: Kwic.fType = Kwic.getKeywordsFromFile
  val stopwords: Kwic.sType = Kwic.getStopWords

  def setStopWordsFile(file: Option[String]) = {
    removeFile("stop_words.txt")
    file match {
      case Some(n) => Files.copy(Paths.get(n), Paths.get("stop_words.txt"))
      case None => ()
    }
  }

  def setIndexFile(file: Option[String]) = {
    removeFile("kwic_index.txt")
    file match {
      case Some(n) => Files.copy(Paths.get(n), Paths.get("kwic_index.txt"))
      case None => ()
    }
  }

  def removeFile(file: String) = {
    if (Files.exists(Paths.get(file))) Files.delete(Paths.get(file))
  }

  override def beforeEach() {
    setStopWordsFile(Some("testFiles" + File.separator + "stop_words.txt"))
  }

  override def afterEach() {
    setStopWordsFile(None)
  }

  "The getStopWords function " should "produce FileNotFoundException when stop_words.txt file does not exist" in {
    setStopWordsFile(None)
    intercept[FileNotFoundException] {
      Kwic.getStopWords()
    }
  }
  it should "return words in lowercase" in {
    setStopWordsFile(Some("testFiles" + File.separator + "upperCaseStopWords.txt"))
    assert(Kwic.getStopWords() == List("upper", "case", "stop", "words"))
  }
  it should "include apostrophes in the words " in {
    setStopWordsFile(Some("testFiles" + File.separator + "apostraphesStopWords.txt"))
    assert(Kwic.getStopWords() == List("can't", "wouldn't"))
  }
  it should "ignore words with numbers or special characters" in {
    setStopWordsFile(Some("testFiles" + File.separator + "numbersAndSpecialStopWords.txt"))
    assert(Kwic.getStopWords() == List("numb", "rs", "special", "characters", "period", "comma", "word"))
  }
  it should "return an empty list if there are no valid words in the file" in {
    setStopWordsFile(Some("testFiles" + File.separator + "noWordsStopFile.txt"))
    assert(Kwic.getStopWords().isEmpty)
  }

  "The getKeywordsFromFile function " should " return an empty list when no keywords found" in {
    assert(Kwic.getKeywordsFromFile("testFiles" + File.separator + "stop_words.txt", Kwic.getStopWords()).isEmpty)
  }
  it should "return an empty list when the file is empty" in {
    assert(Kwic.getKeywordsFromFile("testFiles" + File.separator + "emptyFile.txt", Kwic.getStopWords()).isEmpty)
  }
  it should "only enter each entry once, and in number order" in {
    val exp = List(("football", (List(1, 2))))
    assert(Kwic.getKeywordsFromFile(testLoc + "repeatedWords.txt", Kwic.getStopWords()) == exp)
  }
  it should "not duplicate entries when word is found on the same line" in {
    val exp = List(("football", List(1)))
    assert(Kwic.getKeywordsFromFile(testLoc + "dupOnSameLine.txt", Kwic.getStopWords()) == exp)
  }
  it should "not match with only partial word matches" in {
    val exp = List(("foot", List(3)), ("football", List(1)), ("footballs", List(2)))
    assert(Kwic.getKeywordsFromFile(testLoc + "partialWords.txt", Kwic.getStopWords()) == exp)
  }
  it should "return words in alphbetical order" in {
    val exp = List(("cricket", List(2)), ("football", List(1)), ("rugby", List(1, 2)))
    assert(Kwic.getKeywordsFromFile(testLoc + "alphabetical.txt", Kwic.getStopWords()) == exp)
  }
  it should "match regardless of case" in {
    val exp = List(("football", List(1)))
    assert(Kwic.getKeywordsFromFile(testLoc + "mixedCase.txt", Kwic.getStopWords()) == exp)
  }
  it should "match words with apostrophes" in {
    val exp = List(("'footballs", List(1)), ("football's", List(1)), ("footballs", List(1)), ("footballs'", List(1)))
    assert(Kwic.getKeywordsFromFile(testLoc + "apotraphes.txt", Kwic.getStopWords()) == exp)
  }
  it should "cleanse numerics and special characters from the input file" in {
    val exp = List(("wo", List(1, 2)), ("word", List(3, 4))) //rd is in the list of stopwords
    assert(Kwic.getKeywordsFromFile(testLoc + "numericAndSpecial.txt", Kwic.getStopWords()) == exp)
  }
  it should "exclude a word in the stopFile" in {
    assert(Kwic.getKeywordsFromFile(testLoc + "stop_words.txt", Kwic.getStopWords()).isEmpty)
  }
  it should "match a stopFile even if uppercase" in {
    assert(Kwic.getKeywordsFromFile(testLoc + "upperNumericAndSpecialStopMatch.txt", Kwic.getStopWords()).isEmpty)
  }
  it should "only exclude words in the stopFile" in {
    val exp = List(("football", List(1, 2)))
    assert(Kwic.getKeywordsFromFile(testLoc + "moreThanStopWords.txt", Kwic.getStopWords()) == exp)
  }

  "The printKeywordsOnLine function" should "return empty if the keyword is not found" in {
    val l = "this is a line not containing a wo...rd"
    assert(Kwic.printKeywordsOnLine(w, l, 1) == "")
  }
  it should "match regardless of case, and display original case for the keyword and line" in {
    val word = "woRd"
    val l = "This is a Word."
    assert(Kwic.printKeywordsOnLine(word, l, 1) == "1                     This is a  Word.\n")
  }
  it should "display one instance properly with whitespace before it" in {
    val l = "this is a word."
    assert(Kwic.printKeywordsOnLine(w, l, 1) == "1                     this is a  word.\n")
  }
  it should "find a keyword when it is the end of the line" in {
    val l = "this is a word"
    assert(Kwic.printKeywordsOnLine(w, l, 1) == "1                     this is a  word\n")
  }
  it should "print the right amount of characters before the word when there is more than the required amount before it" in {
    val l = "this is a much longer sentence than the limit is, so it should be cut according before the word string."
    assert(Kwic.printKeywordsOnLine(w, l, 1) == "1 d be cut according before the  word string.\n")
  }
  it should "print only the right amount of characters after the word when there is more than the limit after it" in {
    val l = "after the word string there is a lot of stuff, so it will need to be cut at the limit"
    assert(Kwic.printKeywordsOnLine(w, l, 1) == "1                     after the  word string there is a lot of stuf\n")
  }
  it should "treat special non-apostrophe characters as whitespace" in {
    val l = "whitespace#word_whitespace"
    assert(Kwic.printKeywordsOnLine(w, l, 1) == "1                    whitespace# word_whitespace\n")
  }
  it should "treat numerics as whitespace" in {
    val l = "whitespace9word9whitespace"
    assert(Kwic.printKeywordsOnLine(w, l, 1) == "1                    whitespace9 word9whitespace\n")
  }
  it should "match when the last word is a keyword" in {
    val l = "finish with word"
    assert(Kwic.printKeywordsOnLine(w, l, 1) == "1                   finish with  word\n")
  }
  it should "match when the first word is a keyword" in {
    val l = "word at the start"
    assert(Kwic.printKeywordsOnLine(w, l, 1) == "1                                word at the start\n")
  }
  it should "not match a partial word" in {
    val l = "I spelt forword wrong, I need to improve spelling words.\n"
    assert(Kwic.printKeywordsOnLine(w, l, 1).isEmpty)
  }
  it should "match apostrophes" in {
    val word = "can't"
    val l = "if I can't find this, I'm not working"
    assert(Kwic.printKeywordsOnLine(word, l, 1) == "1                          if I  can't find this, I'm not working\n")
  }
  it should "print multiple instances correctly" in {
    val l = "This contains the word, word twice"
    assert(Kwic.printKeywordsOnLine(w, l, 1) == "1             This contains the  word, word twice\n1       This contains the word,  word twice\n")
  }

  "The getKWICFromFile function" should "throw an FileNotFoundException if the read in file is not found" in {
    intercept[FileNotFoundException] {
      Kwic.getKWICFromFile("FileNotExist", printer, finder, stopwords)
    }
  }
  it should "print no keywords found if the file doesn't contain any keywords" in {
    val soln = testLoc + "emptyFile.txt:\n    No Keywords Found\n"
    assert(Kwic.getKWICFromFile(testLoc + "emptyFile.txt", printer, finder, stopwords) == soln)
  }
  it should "throw a fileNotFoundException if the stopWords file is not found" in {
    setStopWordsFile(None)
    intercept[FileNotFoundException] {
      Kwic.getKWICFromFile(testLoc + "mixedCase.txt", printer, finder, stopwords)
    }
  }
  it should "combine the functions together and print the filename properly" in {
    val soln = testLoc + "all.txt:\n" +
      "3                                'quotationmarks'\n" +
      "2                                cricket9football a\n" +
      "1                                football can't rugby\n" +
      "2                       cricket9 football a\n" +
      "1                football can't  rugby\n"
    assert(Kwic.getKWICFromFile(testLoc + "all.txt", printer, finder, stopwords) == soln)
  }

  "The update KWIC function" should "throw a FileNotFoundException if the stopwords file does not exist, without creating the output file" in {
    setStopWordsFile(None)
    setIndexFile(None)
    intercept[FileNotFoundException] {
      Kwic.updateKWIC(Some(List(testLoc + "emptyFile.txt")))
    }
    assert(!Files.exists(Paths.get("kwic_index.txt")))
  }
  it should "throw a FileNotFoundException if the first file in does not exist, without creating the output file" in {
    setIndexFile(None)
    intercept[FileNotFoundException] {
      Kwic.updateKWIC(Some(List("Doesn't exist")))
    }
    assert(!Files.exists(Paths.get("kwic_index.txt")))
  }
  it should "throw a FileNotFoundException if the second file does not exist, without creating the output file" in {
    setIndexFile(None)
    intercept[FileNotFoundException] {
      Kwic.updateKWIC(Some(List(testLoc + "all.txt","Doesn't exist")))
    }
    assert(!Files.exists(Paths.get("kwic_index.txt")))
  }
  it should "append to an output file that already exists" in {
    val soln = List("football football" + testLoc + "all.txt:","3                                'quotationmarks'","2                                cricket9football a",
      "1                                football can't rugby","2                       cricket9 football a","1                football can't  rugby")
    setIndexFile(Some(testLoc + "dupOnSameLine.txt"))
    Kwic.updateKWIC(Some(List(testLoc + "all.txt")))
    assert(getLinesFromFile("kwic_index.txt") == soln)
  }
  it should "create the new output file it it doesn't already exist" in {
    setIndexFile(None)
    Kwic.updateKWIC(Some(List(testLoc + "all.txt")))
    assert(Files.exists(Paths.get("kwic_index.txt")))
  }
  it should "create the correct output for 1 file being provided" in {
    setIndexFile(None)
    Kwic.updateKWIC(Some(List(testLoc + "all.txt")))
    val soln = List(testLoc + "all.txt:","3                                'quotationmarks'","2                                cricket9football a",
      "1                                football can't rugby","2                       cricket9 football a","1                football can't  rugby")
    assert(getLinesFromFile("kwic_index.txt") == soln)
  }
  it should "create the correct output for 2 files being provided" in {
    setIndexFile(None)
    val soln = List(testLoc + "all.txt:","3                                'quotationmarks'","2                                cricket9football a",
      "1                                football can't rugby","2                       cricket9 football a","1                football can't  rugby")

    Kwic.updateKWIC(Some(List(testLoc + "all.txt",testLoc + "all.txt")))
    assert(getLinesFromFile("kwic_index.txt") == (soln:::soln))
  }
  it should "not create a file if no files are input" in {
    setIndexFile(None)
    Kwic.updateKWIC(None)
    assert(!Files.exists(Paths.get("kwic_index.txt")))
  }
  it should "not append to a file if no files are input" in {
    setIndexFile(Some(testLoc + "all.txt"))
    Kwic.updateKWIC(None)
    assert(getLinesFromFile("kwic_index.txt") == getLinesFromFile(testLoc + "all.txt"))
  }
  it should "do the file output in the order the files are provided" in {
    setIndexFile(None)
    val soln = List(testLoc + "emptyFile.txt:","    No Keywords Found",testLoc + "emptyFile2.txt:","    No Keywords Found")
    Kwic.updateKWIC(Some(List(testLoc + "emptyFile.txt",testLoc + "emptyFile2.txt")))
    assert(getLinesFromFile("kwic_index.txt") == soln)
  }


  private def getLinesFromFile(file:String) : List[String] = {
    val source = Source.fromFile(file)
    val lines = source.getLines().toList
    source.close()
    lines
  }
}



