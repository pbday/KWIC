/**
  * This is the file for the KWIC program as outlined by the coursework specification. It indexes files.
  *
  * This full program can be run by running the object Kwic, which is an App. It can also be run by evoking
  * the Kwic.runKWIC function.
  *
  * The object "Settings" contains the parameters which can be altered for flexibility:
  *   -STOP_FILE: The file containing the stop words
  *   -OUT_FILE: Where the index file should be saved
  *   -PRECEDING_CHARS: The amount of characters to preceed the keyword in the indexed output
  *   -FOLLOWING_CHARS: The amount of characters to follow the keyword in the indexed output
  *   -PRINTER: The function to print out a keyword
  *   -FINDER: The function to find keywords
  *   -STOPWORDS: The function to get the list of stopwords
  *
  * The program relies on the following main functions:
  *   -runKwic: Runs the full program including GUI
  *   -getFilesFromUser: Opens up a file select menu and outputs the list of files the user selected
  *   -updateKWIC: generates full output to the output file, given a list of files to index
  *   -getKWICFromFile: Provides the output from indexing one file as a string
  *   -printKeywordsOnLine: Given a line and the keywords, returns the string of the keywords in context formatted
  *   -getKeywordsFromFile: Extracts all the keywords from a file
  *   -getStopWords: Gets a list of all stop words.
  *
  * The corresponding testing file is "kwic_test".
  *
  */

import java.io.FileWriter

import scala.collection.mutable
import scala.io.Source


object Settings {
  val STOP_FILE = "stop_words.txt"
  val OUT_FILE = "kwic_index.txt"

  val PRECEDING_CHARS = 30
  val FOLLOWING_CHARS = 30

  val PRINTER:Kwic.pType = Kwic.printKeywordsOnLine
  val FINDER:Kwic.fType = Kwic.getKeywordsFromFile
  val STOPWORDS:Kwic.sType = Kwic.getStopWords
}

object Kwic extends App{

  runKWIC()

  def runKWIC() = {
    updateKWIC(getFilesFromUser(None))
  }

  /**
    * Requests files from the user in a prompt box. Files selected are returned in a list.
    * @param alreadySelected The current list. Set to None the first time the method is run.
    * @return An optional list of filenames as strings selected by the user.
    */
  def getFilesFromUser(alreadySelected:Option[List[String]]) : Option[List[String]] = {
    val next = Utility.choosePlainFile("Select another file to index")
    next match {
      case None => alreadySelected
      case Some(f) => alreadySelected match {
                            case Some(c) => getFilesFromUser(Some(c ::: List(f.toString)))
                            case None => getFilesFromUser(Some(List(f.toString)))
                                            }
                }
    }


  /**
    * Takes an optional list of files and outputs the KWIC index to the output file.
    * @param files The optional list of files to be indexed, as a list of strings.
    */

  def updateKWIC(files:Option[List[String]])  = files match {
    case Some(f) => printToOutFile(f.foldLeft("")((s,x) => s + getKWICFromFile(x,Settings.PRINTER,Settings.FINDER,Settings.STOPWORDS)))
    case None => ()
  }

  private def printToOutFile(s:String)  = {
    val writer = new FileWriter(Settings.OUT_FILE,true)
    try{ writer.write(s)}
    finally writer.close()
    }



  type pType = (String,String,Int) => String //Function type that prints the keywords to a string
  type fType = (String, List[String]) => List[(String,List[Int])] //Function type that finds the keywords in a file
  type sType = () => List[String] //Function type that provides the list of stopwords

  /**
    * Given a file will find all keywords which are not stopwords and then output the index for them
    * in the required format as a string ready to be printed to file.
    *
    * @param file The path, as a string to the file to be indexed
    * @param printer The function that given a word prints the output
    * @param finder The function that finds the keywords in a file
    * @param stopwords The function that provides the list of stopwords
    * @return The KWIC for the file formatted in a string
    */
  def getKWICFromFile(file:String,printer:pType,finder:fType,stopwords:sType):String = {
    val fileLines = getLinesFromFile(file).toArray

    val keywords = finder(file,stopwords())

    if (keywords.isEmpty) file + ":\n    No Keywords Found\n"
    else keywords.map(x => printAllLines(printer,x._1,fileLines,x._2)).foldLeft(file+":\n")(_+_)
  }

  private def printAllLines(printer:pType,word:String,lines:Array[String],lineNums:List[Int]): String = {
    lineNums.foldLeft("")((r, x) => r + printer(word,lines(x-1),x))
  }


  /**
    * Prints the keyword and its surrounding line for all instances of the keyword found on the line, to
    * the format specified by the coursework. There is one line for each instance of the word, and it is printed
    * with a new line at the end to allow easy combining of multiple keywords.
    *
    * It was not specified that the space for line numbers should be consistent (should they have more digits), so
    * should line numbers have a different amount of digits then the keywords will not line up when indexed.
    *
    * @param word The keyword
    * @param line The line of text
    * @param lineNum The line number to be printed
    * @return The formatted output for all instances of the keyword on this line, or the empty string it it isnt found
    */
  def printKeywordsOnLine(word:String,line:String,lineNum:Int): String ={
    printKeywordsOnLineAux(word,line,lineNum,0,"")
  }

  private def printKeywordsOnLineAux(word:String,line:String, lineNum:Int, i:Int, current:String) : String =
  {
    val index = line.toLowerCase.indexOf(word.toLowerCase,i)
    if (index == -1) return current
    else if (partOfSubWord(index,line,word)) return printKeywordsOnLineAux(word,line,lineNum, index+1,current)

    val newLine = lineNum + " " + padding(Settings.PRECEDING_CHARS - index) + line.substring(math.max(0,index - Settings.PRECEDING_CHARS),index) + " " +
      line.substring(index,math.min(index + word.length + Settings.FOLLOWING_CHARS,line.length)) + "\n"
    printKeywordsOnLineAux(word,line,lineNum,index+1,current + newLine)
  }

  private def partOfSubWord(index:Int,line:String,word:String) : Boolean ={
    ( !(index == 0 || isDefWhitespace(line.charAt(index - 1))) //word is not first in line nor has whitespace infront of it
      || !( (index + word.length == line.length) || isDefWhitespace(line.charAt(index + word.length)))) //word is not at end nor has whitespace after it
  }
  private def padding(i:Int):String = if (i <= 0) "" else " " + padding(i-1)
  private def isDefWhitespace(c:Char) : Boolean = !(c.isLetter || c == '\'')



  /**
    * Takes a file and a list of stopwords and returns all the keywords found along with thier line
    * number occurances. Multiple instances on a line are not repeated. Words are stored in lowercase.
    * Numbers and all non-apostaphe special characters are are cleansed form the file input and count as spaces.
    *
    * The returned list is in alphabetical order and lines shown in increasing order.
    *
    * @param file The string noting where the file is to be scanned for keywords.
    * @param stopWords The list of stop words, in lower case.
    * @return A list of tuples, the first part being the word and the second being a list of line numbers where it was found
    */
  def getKeywordsFromFile(file:String, stopWords:List[String]):List[(String,List[Int])]  = {

    val outMap = getLinesFromFile(file).map(x => getWordsFromText(x.toLowerCase).filterNot(stopWords.contains)).zipWithIndex.foldLeft(mutable.Map.empty[String,List[Int]])((map,keys) => applyKeysToMap(keys._1,map,keys._2 + 1))

    outMap.toList.sortBy(x => x._1)
  }

  private def applyKeysToMap(keys:List[String],m:mutable.Map[String,List[Int]], l:Int):mutable.Map[String,List[Int]]= {
  keys.foldLeft(m)( (map, s) => map.get(s) match {
    case Some(list) => map += (s -> list.::(l).distinct)
    case None => map += (s -> List(l))
  }).map(x => (x._1,x._2.sorted))

  }

  /**
    * Gets the list of stop words from the stop file (in lower case), or throws file not found exception if it isn't found
    *
    * Numbers and non-apostrophe special characters are ignored and treated as white space.
    *
    * @return The list of words from the stop words file, in lower case.
    */
  def getStopWords():List[String] = {
    getWordsFromFile(Settings.STOP_FILE).map(w => w.toLowerCase)}

  private def getLinesFromFile(file:String) : List[String] = {
    val source = Source.fromFile(file)
    val lines = source.getLines().toList
    source.close()
    lines
  }

  private def getWordsFromFile(file:String) : List[String] = {
    val source = Source.fromFile(file)
    val fileString = source.mkString
    source.close()
    getWordsFromText(fileString)}

  /**
    * Returns words from a string, including apostrophes as well letters. This is an adaption
    * of Keith's original Utilities method to include apostrophes.
    *
    * @param text A string of words
    * @return A list of all words in the input string
    */

  def getWordsFromText(text: String): List[String] = {
    ("""[a-zA-Z']+""".r findAllIn text) toList
  }

}
