This is the keywords in context (KWIC) as part of the portfolio submission for the PPL course.

It consists of the following:

* src/kwic.scala: The main program, which can be run as an app. Details are contained in comments at the top of the program.

* src/kwic_test.scala: The test program containing all the tests for the kwic program. Details can be found in the comments at the top of the program.

* testFiles/: A directory containing text files required for the tests to run. These should not be moved to a different directory as the test will access these directly.

* src/Utility.scala: Utility functions provided by Keith

These require the swing and scalatest external libraries to run.